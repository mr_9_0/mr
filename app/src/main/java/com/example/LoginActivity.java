package com.example;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.database.StudentMessage;
import com.example.entity.Student;

public class LoginActivity extends AppCompatActivity {
    private StudentMessage studentMessage;
    private EditText nameEditText;
    private EditText studentIDEditText;
    private EditText passwordEditText;
    private Button loginButton;
    private Button signButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        nameEditText = findViewById(R.id.nameEditText);
        studentIDEditText = findViewById(R.id.studentIDEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        loginButton = findViewById(R.id.loginButton);
        signButton = findViewById(R.id.signButton);
        studentMessage = new StudentMessage(this);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String studentName = nameEditText.getText().toString().trim();
                String studentId = studentIDEditText.getText().toString().trim();
                String password = passwordEditText.getText().toString().trim();

                //使用SharedPreferences：在登录成功后将学生ID和学生姓名信息保存到SharedPreferences中，然后在其他界面中读取这些信息。
                SharedPreferences sharedPreferences = getSharedPreferences("loginIdAndName" , Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("studentId" , studentId);
                editor.putString("studentName" , studentName);
                editor.apply();//使用apply异步保存，使用commit同步保存

                // 非空校验
                if (studentName.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "请输入姓名", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (studentId.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "请输入学号", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "请输入密码", Toast.LENGTH_SHORT).show();
                    return;
                }

                // 学号格式校验（假设学号为数字且有特定长度）
                if (!studentId.matches("\\d{11}")) {
                    Toast.makeText(LoginActivity.this, "学号应为9位数字", Toast.LENGTH_SHORT).show();
                    return;
                }

                // 密码强度校验（可根据需要调整）
                if (password.length() < 6) {
                    Toast.makeText(LoginActivity.this, "密码至少需要6位字符", Toast.LENGTH_SHORT).show();
                    return;
                }

                // 尝试登录
                Student student = studentMessage.getStudent(studentId, password);
                if (student != null) {
                    // 额外确认姓名匹配，尽管这通常在getStudent逻辑中已涵盖，这里作为演示
                    if (student.getStudentName().equals(studentName)) {
                        Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, StudentActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "姓名与学号密码不匹配", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "学号或密码错误", Toast.LENGTH_SHORT).show();
                }
            }
        });


        signButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this , SignActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
