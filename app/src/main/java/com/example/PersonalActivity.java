package com.example;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.database.StudentMessage;
import com.example.view.Customize;
import com.example.view.CustomizeBottom;
import com.example.view.Customize_personal;

public class PersonalActivity extends AppCompatActivity {
    private StudentMessage studentMessage ;
    private Customize_personal customize_personal ;
    private CustomizeBottom customizeBottom ;
    private LinearLayout per_linear ;
    private TextView per_name ;
    private TextView per_id ;
    private TextView per_chooseCourse ;
    private TextView per_setting ;
    private ImageView avatar ;
    @SuppressLint({"MissingInflatedId", "SetTextI18n"})
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal);
        customize_personal = findViewById(R.id.per_top);
        customizeBottom = findViewById(R.id.per_bottom);
        per_linear=findViewById(R.id.per_linear);
        per_name = findViewById(R.id.per_name);
        per_id = findViewById(R.id.per_id) ;
        per_chooseCourse=findViewById(R.id.per_chooseCourse);
        per_setting = findViewById(R.id.per_setting);
        avatar = findViewById(R.id.avatar);

        //获取登录页面传来的id和name
        SharedPreferences sharedPreferences = getSharedPreferences("loginIdAndName" , Context.MODE_PRIVATE);
        String studentId = sharedPreferences.getString("studentId" , "");
        String studentName = sharedPreferences.getString("studentName" , "") ;

        customize_personal.setLeftBtnNoVisible(true);//不显示
        customize_personal.setRightBtnNoVisible(true);
        customize_personal.setTitleText("我");
        customizeBottom.setPersonBtnBackground(R.color.bottomUse);
        customizeBottom.setStudentBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalActivity.this , StudentActivity.class) ;
                startActivity(intent);
                finish();
            }
        });
        customizeBottom.setCourseBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalActivity.this , CourseActivity.class) ;
                startActivity(intent);
                finish();
            }
        });

        per_name.setText("姓名：" + studentName);
        per_id.setText("学号" + studentId);
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalActivity.this , InformationActivity.class);
                startActivity(intent);
            }
        });
        per_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalActivity.this , InformationActivity.class);
                startActivity(intent);
            }
        });
        per_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalActivity.this , InformationActivity.class);
                startActivity(intent);
            }
        });

        per_chooseCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalActivity.this , ChooseCourse.class);
                startActivity(intent);
            }
        });
        per_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalActivity.this , SettingActivity.class);
                startActivity(intent);
            }
        });
    }
}
