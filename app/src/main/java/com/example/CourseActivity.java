package com.example;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.view.Customize_course;
import com.example.view.CustomizeBottom;
import com.example.view.Customize_course;

public class CourseActivity extends AppCompatActivity {
    private Customize_course customize_course ;
    private CustomizeBottom customizeBottom ;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.array_course1);
        customize_course = findViewById(R.id.student_top);
        customizeBottom = findViewById(R.id.student_bottom);
        customize_course.setLeftBtnNoVisible(true);//不显示
        customize_course.setRightBtnNoVisible(true);
        customize_course.setTitleText("课程");
        customizeBottom.setCourseBtnBackground(R.color.bottomUse);
        customizeBottom.setStudentBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CourseActivity.this , StudentActivity.class) ;
                startActivity(intent);
                finish();
            }
        });
        customizeBottom.setPersonBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CourseActivity.this , PersonalActivity.class) ;
                startActivity(intent);
                finish();
            }
        });
    }
}
