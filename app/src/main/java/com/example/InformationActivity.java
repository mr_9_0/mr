package com.example;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.database.StudentMessage;
import com.example.entity.Student;
import com.example.view.Customize;

public class InformationActivity extends AppCompatActivity {
    private StudentMessage studentMessage ;
    private Customize customize ;
    private TextView perName ;
    private TextView perId ;
    private TextView perSex ;
    private  TextView perCourse ;
    private TextView perPassword ;
    private Button cancel_button ;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information);
        customize = findViewById(R.id.Details);
        perName = findViewById(R.id.perName);
        perId = findViewById(R.id.perId);
        perSex = findViewById(R.id.perSex);
        perCourse = findViewById(R.id.perCourse);
        perPassword = findViewById(R.id.perPassword);
        cancel_button = findViewById(R.id.cancelButton);

        customize.setRightBtnNoVisible(true);
        customize.setLeftBtnNoVisible(true);
        customize.setTitleText("我的信息");
        //获取登录页面传递的登录信息并通过查询id来查询班级
         studentMessage = new StudentMessage(this);
        //获取登录页面传递的登录信息
        SharedPreferences sharedPreferences = getSharedPreferences("loginIdAndName" , Context.MODE_PRIVATE);
        String studentId = sharedPreferences.getString("studentId", "-1");
        Student student = studentMessage.getStudentById(studentId);
        if (student != null){
            perName.setText("姓名：" + student.getStudentName());
            perId.setText("学号：" + studentId);
            perSex.setText("性别：" + student.getSex());
            perCourse.setText("班级：" + student.getCourse());
            perPassword.setText("密码：" + student.getPassword());
        }else {
            Toast.makeText(this, "未找到学生信息", Toast.LENGTH_SHORT).show();
        }
//            perName.setText(String.format("姓名：%s", student.getStudentName()));
//           perId.setText("学号：" + studentId);
//            perSex.setText(String.format("性别：%s", student.getSex()));
//            perCourse.setText(String.format("班级：%s", student.getCourse()));
//            perPassword.setText(String.format("密码：%s", student.getPassword()));
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
