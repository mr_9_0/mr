package com.example;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.database.StudentMessage;
import com.example.entity.Student;
import com.example.view.Customize;
import com.example.view.CustomizeBottom;
import com.example.view.Customize_friends;

import java.util.List;

public class StudentActivity extends AppCompatActivity implements StudentAdapter.OnStudentDeleteListener{
    private Customize_friends customize_friends;
    private CustomizeBottom customizeBottom;
    private RecyclerView recyclerView ;
    private StudentAdapter adapter;
    private List<Student> students;
    private StudentMessage studentMessage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.array_student1);
        recyclerView = findViewById(R.id.recyclerViewCourses);
        customize_friends = findViewById(R.id.student_top);
        customizeBottom = findViewById(R.id.student_bottom);

        studentMessage = new StudentMessage(this);
        students = studentMessage.getAllStudent();
        adapter = new StudentAdapter(this,students,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        customize_friends.setLeftBtnNoVisible(true);//不显示
        customize_friends.setRightBtnNoVisible(true);
        customize_friends.setTitleText("同学");
        customizeBottom.setStudentBtnBackground(R.color.bottomUse);
        customizeBottom.setCourseBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StudentActivity.this, CourseActivity.class);
                startActivity(intent);
                finish();
            }
        });
        customizeBottom.setPersonBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StudentActivity.this, PersonalActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    @Override
    public void onStudentDeleted(Student student) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("确认删除");
        builder.setMessage("您确定要删除姓名 "+ student.getStudentId()+" 这条信息吗？" );
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // 调用数据库操作来删除学生记录
                boolean isDeleted = studentMessage.deleteStudentByName(student.getStudentId());
                if (isDeleted) {
                    // 从适配器的数据列表中移除学生
                    int position = students.indexOf(student);
                    if (position != -1) {
                        students.remove(position);
                        adapter.notifyItemRemoved(position); // 通知适配器移除该项
                        adapter.updateStudentList(students); // 更新适配器中的学生列表
                        Toast.makeText(StudentActivity.this, "学生信息删除成功", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(StudentActivity.this, "学生信息删除失败", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("取消", null);
        builder.create().show();
    }
}
