package com.example.entity;

import java.io.Serializable;

public class Student implements Serializable {
    private String studentName;
    private String studentId;
    private String password;
    private String sex;
    private String course;

    // 无参构造函数
    public Student() {
    }

    // 包含所有属性的构造函数
    public Student(String studentName, String studentId, String password, String sex, String course) {
        this.studentName = studentName;
        this.studentId = studentId;
        this.password = password;
        this.sex = sex;
        this.course = course;
    }

    public Student(String studentName, String studentId, String password) {
        this.studentName = studentName;
        this.studentId = studentId;
        this.password = password;
    }

    public Student(String studentName, String sex, String course, String password) {
        this.studentName = studentName;
        this.studentId = studentId;
        this.password = password;
        this.sex = sex;
    }

    // Getters和Setters
    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    // toString 方法，方便调试时输出
    @Override
    public String toString() {
        return "Student{" +
                "studentName='" + studentName + '\'' +
                ", studentId='" + studentId + '\'' +
                ", password='" + password + '\'' +
                ", sex='" + sex + '\'' +
                ", course='" + course + '\'' +
                '}';
    }
}