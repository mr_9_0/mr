package com.example.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.entity.Student;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;


public class StudentMessage extends SQLiteOpenHelper {
    private static final String DB_Name = "school" ;
    private static final int DB_Version = 1 ;

    public StudentMessage(@Nullable Context context) {
        super(context, DB_Name, null, DB_Version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE students (" +
                "studentName TEXT NOT NULL," +
                "studentId TEXT PRIMARY KEY ," +
                "password TEXT NOT NULL," +
                "sex TEXT ," +
                "course TEXT )" ;
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS students");
        onCreate(db);
    }

    @SuppressLint("Range")
    public Student getStudent(String studentId , String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("students" ,
                new String[]{"studentName" , "studentID" ,"password"},
                "studentId =? AND password =?" ,
                new String[]{studentId , password},
                null , null , null , null
        );
        Student student = null ;
        if (cursor != null && cursor.moveToFirst()){
            student = new Student(
                    cursor.getString(cursor.getColumnIndex("studentName")),
                    cursor.getString(cursor.getColumnIndex("studentId")),
                    cursor.getString(cursor.getColumnIndex("password"))
            );
            cursor.close();
        }
        return student;
    }

    public long insertStudent(Student student){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues() ;
        contentValues.put("studentName" ,student.getStudentName() );
        contentValues.put("studentId" , student.getStudentId());
        contentValues.put("password" , student.getPassword());
        contentValues.put("sex" , student.getSex());
        contentValues.put("course" , student.getCourse());
        long students = db.insert("students" ,null , contentValues) ;
        return students ;
    }

    //设置界面通过登陆界面获取的studentId来查询班级属性

    public  Student getCourseByStudentId(String studentId){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("students" , new String[]{"course" },
                studentId + "=?" , new String[]{String.valueOf(studentId)},
                null,null,null,null
        );
        Student student = null ;

        return student ;
    }

    //通过查询id是否存在和修改语句来实现密码的修改
    public String getPasswordByStudentID(String studentId){
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT password FROM students WHERE studentId = ?";
        Cursor cursor = db.rawQuery(query,new String[]{studentId});
        if (cursor !=null && cursor.moveToFirst()){
            @SuppressLint("Range") String password = cursor.getString(cursor.getColumnIndex("password"));
            cursor.close();
            return password ;
        }
        return null ;
    }
    public boolean updatePassword(String studentId, String newPassword) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("password", newPassword);
        int rowsAffected = db.update("students", values, "studentId = ?", new String[]{studentId});
        return rowsAffected > 0;
    }
    //查询个人信息

    @SuppressLint("Range")
    public Student getStudentById(String studentId){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT studentName , sex , course , password FROM students WHERE studentId = ?" ;
        Cursor cursor = db.rawQuery(query , new String[]{String.valueOf(studentId)});
        Student student = null ;
        if (cursor.moveToFirst()){
            String studentName = cursor.getString(cursor.getColumnIndexOrThrow("studentName"));
            String sex = cursor.getString(cursor.getColumnIndexOrThrow("sex"));
            String course = cursor.getString(cursor.getColumnIndexOrThrow("course"));
            String password = cursor.getString(cursor.getColumnIndexOrThrow("password"));
            student = new Student(studentName, sex, course, password);
        }
        cursor.close();
        return student ;
    }
    @SuppressLint("Range")
    public List<Student> getAllStudent(){
        List<Student> students = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("students" , new String[]{"studentName" , "studentId" , "password" , "sex" ,"course" } ,
                null , null , null , null ,null);
        if (cursor != null && cursor.moveToFirst()){
            do {
                String studentName = cursor.getString(cursor.getColumnIndex("studentName"));
                String studentId = cursor.getString(cursor.getColumnIndex("studentId"));
                String password = cursor.getString(cursor.getColumnIndex("password"));
                String sex = cursor.getString(cursor.getColumnIndex("sex"));
                String course = cursor.getString(cursor.getColumnIndex("course"));
                students.add(new Student(studentId, studentName , password , sex ,course));
            }while (cursor.moveToNext());
        }
        if (cursor != null){
            cursor.close();
        }
        db.close();
        return students ;
    }

        public boolean deleteStudentByName(String studentName) {
        SQLiteDatabase db = this.getWritableDatabase();
        int rowsDeleted = db.delete("students", "studentName = ?", new String[]{studentName});
        db.close(); // 不要忘记关闭数据库
        return rowsDeleted > 0;
    }

}
