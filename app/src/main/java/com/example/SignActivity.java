package com.example;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.database.StudentMessage;
import com.example.entity.Student;

public class SignActivity extends AppCompatActivity {
    private StudentMessage message ;
    private EditText name_edittext , student_id_edittext ,password_edittext , course_edittext;
    private RadioButton male_radio ,female_radio ;
    private Button resister_button ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign);
        name_edittext=findViewById(R.id.name_edittext);
        student_id_edittext=findViewById(R.id.student_id_edittext);
        password_edittext=findViewById(R.id.password_edittext);
        course_edittext=findViewById(R.id.class_edittext);
        male_radio=findViewById(R.id.male_radio);
        female_radio=findViewById(R.id.female_radio);
        resister_button=findViewById(R.id.register_button);
        message = new StudentMessage(this) ;

    }

    public void sign(View view) {
        String studentName = name_edittext.getText().toString().trim();
        String studentId = student_id_edittext.getText().toString().trim();
        String password = password_edittext.getText().toString().trim();
        String sex = male_radio.isChecked() ? "Male" : "Female";
        String course = course_edittext.getText().toString().trim();

        // 输入校验
        if (studentName.isEmpty() || studentId.isEmpty() || password.isEmpty() || course.isEmpty()) {
            Toast.makeText(this, "所有字段均为必填项", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!studentId.matches("\\d{11}")) {
            Toast.makeText(this, "学生ID必须是9位数字", Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.length() < 6) {
            Toast.makeText(this, "密码至少需要6位字符", Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.length()>16){
            Toast.makeText(this, "密码不能超过16位字符", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!course.matches("[A-Z]{2}\\d{3}")) {
            Toast.makeText(this, "班级代码格式不正确，请使用如CS101的格式", Toast.LENGTH_SHORT).show();
            return;
        }
        if (male_radio.isChecked() == female_radio.isChecked()) {
            Toast.makeText(this, "请选择性别", Toast.LENGTH_SHORT).show();
            return;
        }


        Student student = new Student(studentName , studentId , password , sex , course);
        long insert = message.insertStudent(student);
        // 检查输入是否有效，然后添加学生
        if(insert != -1){
            Toast.makeText(this , "注册成功" , Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(SignActivity.this , LoginActivity.class);
            startActivity(intent);
            finish();;
        }else {
            Toast.makeText(this , "注册失败" , Toast.LENGTH_SHORT).show();
        }
    }
}
