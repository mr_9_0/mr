package com.example;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.database.StudentMessage;
import com.example.entity.Student;

public class SettingActivity extends AppCompatActivity {
    private LinearLayout updatePassword ;
    private LinearLayout exitLogin ;
    private Switch switchNotification ;
    private Switch switchAdminMode ;
    private LinearLayout addStudent ;
    private LinearLayout addCourse ;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);
        updatePassword=findViewById(R.id.updatePassword);
        exitLogin=findViewById(R.id.exitLogin);
        switchNotification=findViewById(R.id.switchNotification);
        switchAdminMode=findViewById(R.id.switchAdminMode);
        addStudent=findViewById(R.id.addStudent);
        addCourse=findViewById(R.id.addCourse);

        addStudent.setVisibility(View.GONE);
        addCourse.setVisibility(View.GONE);

        //获取登录页面传递的登录信息并通过查询id来查询班级
        StudentMessage studentMessage = new StudentMessage(this);

        // Update Password Button Click Event
        updatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, UpdatePasswordActivity.class);
                startActivity(intent);
            }
        });

// Exit Login Button Click Event
        exitLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginOut();
            }
        });

// Switch for Notification Click Event
        switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // User turned on notifications, send a notification here
                    Toast.makeText(SettingActivity.this, "通知功能已打开", Toast.LENGTH_SHORT).show();

                    sendNotification();
                } else {
                    // User turned off notifications, handle accordingly
                    Toast.makeText(SettingActivity.this, "通知功能已关闭", Toast.LENGTH_SHORT).show();
                    // No action needed if not sending anything on disabling
                }
            }
        });
        //获取登录页面传递的登录信息
        SharedPreferences sharedPreferences = getSharedPreferences("loginIdAndName" , Context.MODE_PRIVATE);
        String studentId = sharedPreferences.getString("studentId", "");
        String course = String.valueOf(studentMessage.getCourseByStudentId(studentId));
//        if (course.equals("GL000")) {
        switchAdminMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    // 根据开关状态显示或隐藏addStudent和addCourse
                    addStudent.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    addCourse.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    Toast.makeText(SettingActivity.this, "管理员模式已开启", Toast.LENGTH_SHORT).show();
            }
        });

        addStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this , AddStudentActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loginOut(){
        // 清除保存的登录信息
        SharedPreferences sharedPreferences = getSharedPreferences("login_info", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();

        // 跳转回登录页面
        Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
        startActivity(intent);
        finish(); // 结束当前Activity
    }
    private void sendNotification() {
    }

}
