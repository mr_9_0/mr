package com.example;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.database.StudentMessage;
import com.example.entity.Student;

public class UpdatePasswordActivity extends AppCompatActivity {
    private StudentMessage studentMessage ;
    private EditText student_id ;
    private EditText oldPassword ;
    private EditText newPassword ;
    private EditText confirmPassword ;
    private Button sureUpdate ;
    private Button cancelUpdate ;

    @SuppressLint("MissingInflatedId")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.updatepassword);
        student_id = findViewById(R.id.studentId);
        oldPassword = findViewById(R.id.oldPassword);
        newPassword = findViewById(R.id.newPassword);
        confirmPassword = findViewById(R.id.confirmPassword);
        sureUpdate = findViewById(R.id.sureUpdate);
        studentMessage = new StudentMessage(this);
        cancelUpdate = findViewById(R.id.cancelUpdate);

        sureUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePassword();
            }
        });
        cancelUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private void updatePassword() {
        String studentId = student_id.getText().toString().trim();
        String oldPw = oldPassword.getText().toString().trim();
        String newPw = newPassword.getText().toString().trim();
        String confirmPw = confirmPassword.getText().toString().trim();

        // 基本输入校验
        if (TextUtils.isEmpty(studentId) || TextUtils.isEmpty(oldPw) || TextUtils.isEmpty(newPw) || TextUtils.isEmpty(confirmPw)) {
            Toast.makeText(this, "所有字段都必须填写", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!newPw.equals(confirmPw)) {
            Toast.makeText(this, "新密码和确认密码不匹配", Toast.LENGTH_SHORT).show();
            return;
        }

        // 假设studentMessage是StudentMessage的实例
        String storedPassword = studentMessage.getPasswordByStudentID(studentId);
        //获取登录页面传递的登录信息
        SharedPreferences sharedPreferences = getSharedPreferences("loginIdAndName" , Context.MODE_PRIVATE);
        String student_Id = sharedPreferences.getString("studentId", "");
        if (!studentId.equals(student_Id)){
            Toast.makeText(this, "请本人操作！！！", Toast.LENGTH_SHORT).show();
            return;
        }

        // 数据库查询结果校验
        if (storedPassword == null) {
            Toast.makeText(this, "无效的学生ID或密码错误", Toast.LENGTH_SHORT).show();
            return;
        }

        // 旧密码校验
        if (!storedPassword.equals(oldPw)) {
            Toast.makeText(this, "旧密码不正确", Toast.LENGTH_SHORT).show();
            return;
        }

        // 密码更新
        if (studentMessage.updatePassword(studentId, newPw)) {
            Toast.makeText(this, "密码更新成功", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, "密码更新失败，请重试", Toast.LENGTH_SHORT).show();

        }
    }
}
