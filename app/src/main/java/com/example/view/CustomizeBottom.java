package com.example.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.R;

public class CustomizeBottom extends LinearLayout {
    public LinearLayout customBar ;
    private Button studentBtn ;
    private Button courseBtn ;
    private Button personBtn ;
    public CustomizeBottom(Context context) {
        super(context);
        bottomView();
    }

    public CustomizeBottom(Context context, AttributeSet attrs) {
        super(context, attrs);
        bottomView();
    }
    private void bottomView() {
        View inflatedView = LayoutInflater.from(getContext()).inflate(R.layout.customize__view, this, true);
        // 使用inflatedView来查找子元素
        customBar = inflatedView.findViewById(R.id.customBar);
        studentBtn = inflatedView.findViewById(R.id.studentBtn);
        courseBtn = inflatedView.findViewById(R.id.courseBtn);
        personBtn = inflatedView.findViewById(R.id.personBtn);
    }
    public void setStudentBtnClickListener(OnClickListener listener){
        studentBtn.setOnClickListener(listener);
    }
    public void setCourseBtnClickListener(OnClickListener listener){
        courseBtn.setOnClickListener(listener);
    }
    public void setPersonBtnClickListener(OnClickListener listener){
        personBtn.setOnClickListener(listener);
    }
    public void setStudentBtnBackground(int res){
        studentBtn.setBackgroundColor(res);
    }
    public void setCourseBtnBackground(int res){
        courseBtn.setBackgroundColor(res);
    }
    public void setPersonBtnBackground(int res){
        personBtn.setBackgroundColor(res);
    }
}
