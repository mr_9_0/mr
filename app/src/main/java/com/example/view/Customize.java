package com.example.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.R;

public class Customize extends RelativeLayout {
    private RelativeLayout custom_bar ;
    private Button leftBtn ;
    private TextView titleText ;
    private Button rightBtn ;
    public Customize(Context context) {
        super(context);
        initView();
    }

    public Customize(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView(){
        LayoutInflater.from(getContext()).inflate(R.layout.customize_view, this , true) ;
        custom_bar = findViewById(R.id.custom_bar) ;
        leftBtn = findViewById(R.id.leftBtn) ;
        titleText = findViewById(R.id.title_text) ;
        rightBtn = findViewById(R.id.rightBtn);

    }
    public void setTitleText(String text) {
        if (titleText != null && !TextUtils.isEmpty(text)) {
            titleText.setText(text);
        }
    }
    public  void setLeftBtnText(String text){
        leftBtn.setText(text);
    }
    public void  setRightBtnText(String text){
        rightBtn.setText(text);
    }
    public void setLeftBtnClickListener(OnClickListener listener){
        leftBtn.setOnClickListener(listener);
    }
    public void setRightBtnClickListener(OnClickListener listener){
        rightBtn.setOnClickListener(listener);
    }
    public void setBackground(int res){
        custom_bar.setBackgroundColor(res);
    }
    public void setLeftBtnIsVisible(boolean isVisible){
        leftBtn.setVisibility(VISIBLE);
    }
    public void setLeftBtnNoVisible(boolean isVisible){
        leftBtn.setVisibility(INVISIBLE);
    }
    public void setRightBtnIsVisible(boolean isVisible){
        rightBtn.setVisibility(VISIBLE);
    }
    public void setRightBtnNoVisible(boolean isVisible){
        rightBtn.setVisibility(INVISIBLE);
    }
}
